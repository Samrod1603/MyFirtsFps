using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehavior : MonoBehaviour
{
    public float health, maxHealth , damage;
    [SerializeField] NavMeshAgent navMesh;
    [SerializeField] GameObject player;

    private void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
    }
    void Update()
    {
        if (navMesh != null)
        {
            navMesh.SetDestination(player.transform.position);
        }

        if (health <= 0)
        {
            Death();
        }
    }

    public void GetDamage(float damage)
    {
        print("Enemy Received " + damage + " Damaged");
        health -= damage;
    }

    public void ResetStats()
    {
        health = maxHealth;
    }

    public void Death()
    {
        GameManager.Instance.ReturnToPool(gameObject);
        ResetStats();
        gameObject.SetActive(false);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponentInParent<PlayerStats>().GetDamage(damage);
        }
    }
}
