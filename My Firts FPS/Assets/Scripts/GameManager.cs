using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    [Header("Enemy Spawn")]
    [SerializeField] List<GameObject> enemies;
    public float xBorder, zBorder, spawnTime, initialSpawnTime;
    [SerializeField] GameObject enemyPrefab1, enemyPrefab2;

    static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else _instance = this;

        initialSpawnTime = spawnTime;
    }
    private void Update()
    {
        if (spawnTime <= 0)
        {
            SpawnEnemies();
        }
        else
        {
            spawnTime -= 1 * Time.deltaTime;
        }
    }
    public void SpawnEnemies()
    {
        spawnTime = initialSpawnTime;
        float xSpawnPos = Random.Range(-xBorder, xBorder), zSpawnPos = Random.Range(-zBorder, zBorder);
        print("x" + xSpawnPos + ",z" + zSpawnPos);
        if (enemies.Count == 0)
        {
            print("no hay mas enemigos");
        }
        else
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i].activeInHierarchy == false)
                {
                    enemies[i].SetActive(true);
                    enemies[i].transform.position = new Vector3(xSpawnPos, -0.002f, zSpawnPos);
                    enemies.Remove(enemies[i]);
                    break;
                }
            }
        }

    }

    public IEnumerator ReturnMainMenu()
    {
        yield return new WaitForSeconds(3);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        SceneManager.LoadScene(0);
    }

    public void ReturnToPool(GameObject enemy)
    {
        enemies.Add(enemy);
        enemy.transform.position = transform.position;
    }
}
