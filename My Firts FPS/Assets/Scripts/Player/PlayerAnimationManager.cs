using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationManager : MonoBehaviour
{
    [SerializeField] Animator anima;
    bool isWalking, isJumping, isDead, isRunning, isDodging, isDamaged, isInjured;
    private int clicks;
    private bool canClick;

    #region Accesores
    public bool IsWalking { get => isWalking; set => isWalking = value; }
    public bool IsJumping { get => isJumping; set => isJumping = value; }
    public bool IsDead { get => isDead; set => isDead = value; }
    public bool IsRunning { get => isRunning; set => isRunning = value; }
    public bool IsDodging { get => isDodging; set => isDodging = value; }
    public bool IsDamaged { get => isDamaged; set => isDamaged = value; }
    public bool IsInjured { get => isInjured; set => isInjured = value; }
    #endregion

    private void Start()
    {
        clicks = 0;
        canClick = true;
    }

    private void Update()
    {
        anima.SetBool("IsWalking", isWalking);
        anima.SetBool("IsJumping", isJumping);
        anima.SetBool("IsDodging" , isDodging);
        anima.SetBool("IsRunning" , isRunning);
        anima.SetBool("IsInjured", isInjured);

        if (Input.GetMouseButtonDown(0))
        {
            AttackCombo();
        }
    }

    public void Death()
    {
        anima.SetTrigger("IsDead2");
    }

    private void AttackCombo()
    {
        if (canClick)
        {
            clicks++;
        }

        if (clicks == 1)
        {
            anima.SetInteger("Attack", 1);
        }
    }
    public void CheckCombo()
    {
        canClick = false;

        if (anima.GetCurrentAnimatorStateInfo(0).IsName("Attack1") && clicks == 1)
        {
            anima.SetInteger("Attack", 0);
            canClick = true;
            clicks = 0;
        }
        else if (anima.GetCurrentAnimatorStateInfo(0).IsName("Attack1") && clicks >= 2)
        {
            anima.SetInteger("Attack", 2);
            canClick = true;
        }
        else if (anima.GetCurrentAnimatorStateInfo(0).IsName("Attack2") && clicks == 2)
        {
            anima.SetInteger("Attack", 0);
            canClick = true;
            clicks = 0;
        }
        else if (anima.GetCurrentAnimatorStateInfo(0).IsName("Attack2") && clicks >= 3)
        {
            anima.SetInteger("Attack", 3);
            canClick = true;
        }
        else if (anima.GetCurrentAnimatorStateInfo(0).IsName("Attack3"))
        {
            anima.SetInteger("Attack", 0);
            canClick = true;
            clicks = 0;
        }
    }

}
