using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerController : MonoBehaviour
{
    InputManager inputManager;
    [Header("Movement")]
    [SerializeField] CharacterController characterController;
    [SerializeField] float speed, dodgeTime, dodgeSpeed, startSpeed;
    [SerializeField] float turnSmoothTime = 0.1f, turnSmoothVelocity = 0.1f;
    [SerializeField] GameObject damageCapsule;

    [Header("Animations")]
    [SerializeField] PlayerAnimationManager animationManager;
    [SerializeField] PlayerStats stats;

    [Header("Camera Movement")]
    [SerializeField] Transform cameraTransform, playerBody;
    [SerializeField] CinemachineFreeLook vCamera;

    [Header("Physics")]
    [SerializeField] Transform groundCheck;
    [SerializeField] float gravityValue, jumpHeigh, groundDistance, mass;
    [SerializeField] LayerMask groundMask;
    bool isGrounded;
    Vector3 weight, move, verticalVelocity;

    [Header("CheckCollisions")]
    [SerializeField] float rayMaxDistance;
    [SerializeField] float rayOffset;

    //private Vector3 rightFootPosition, leftFootPosition, rightFootIkPosition, leftFootIkPosition;
    //private Quaternion rightFootIkRotation, leftFootIkRotation;
    //private float lastPelvisPositionY, lastRightFootPositionY, lastLeftFootPositionY;

    //[Header("Feet Grounder")]
    //public bool enableFeetIk = true;
    //[SerializeField][Range(0, 2)] private float heightFromGroundRaycast = 1.14f;
    //[SerializeField][Range(0, 2)] private float raycastDownDistance = 1.5f;
    //[SerializeField] private LayerMask environmentLayer;
    //[SerializeField] private float pelvisOffset = 0f;
    //[SerializeField][Range(0, 1)] private float pelvisUpandDownSpeed = 0.28f;
    //[SerializeField][Range(0, 1)] private float feetToIkPositionSpeed = 0.5f;

    //public string leftFootAnimVariableName = "LeftFootCurve";
    //public string rightFootAnimVariableName = "RightFootCurve";

    //public bool useProIkFeature = false;
    //public bool showSolverDebug = true;

    private void Start()
    {
        inputManager = InputManager.Instance;
        startSpeed = speed;
    }

    private void Update()
    {
        if (stats.health <= 0)
        {
            stats.Death();
            characterController.enabled = false;
            damageCapsule.SetActive(false);
        }
        else if (stats.health > 0)
        {
            #region Movement
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            Vector2 inputMovement = inputManager.GetPlayerMovement();
            move = new Vector3(inputMovement.x, 0f, inputMovement.y);
            move = cameraTransform.forward * move.z + cameraTransform.right * move.x;
            move.y = 0;
            CalculateWeight();

            #region Walk
            if (move.magnitude >= 0.1f)
            {
                if (move.y == 0 && speed == startSpeed)
                {
                    if (stats.health <= stats.maxHealth * 0.6f) //if health is less or equal than 60% of health
                    {
                        animationManager.IsInjured = true;
                    }
                    else
                    {
                        animationManager.IsWalking = true;
                    }

                }
                float targetAngle = Mathf.Atan2(move.x, move.z) * Mathf.Rad2Deg;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0, angle, 0);
            }
            else
            {
                if (stats.health <= stats.maxHealth * 0.6f) //if health is less or equal than 60% of health
                {
                    animationManager.IsInjured = false;
                }
                else
                {
                    animationManager.IsWalking = false;
                }
            }//if stop moving
            #endregion

            #region Dodge
            if (inputManager.isPlayerDodging())
            {
                animationManager.IsDodging = true;
                StartCoroutine(Dodge());
            }
            else animationManager.IsDodging = false;
            #endregion

            #region Run
            if (inputManager.isPlayerRunning())
            {
                speed = startSpeed * 1.5f;
                animationManager.IsRunning = true;
            }
            else
            {
                speed = startSpeed;
                animationManager.IsRunning = false;
            }
            #endregion

            characterController.Move(verticalVelocity * Time.deltaTime);
            characterController.Move(move * speed * Time.deltaTime);
            #endregion
        }
    }
    public void CalculateWeight()
    {
        if (isGrounded)
        {
            weight = Vector3.zero;
            if (inputManager.IsPlayerJumping())
            {
                animationManager.IsJumping = true;
                verticalVelocity.y = Mathf.Sqrt(jumpHeigh * -2f * gravityValue);
            }
        }
        else
        {
            animationManager.IsJumping = false;
            verticalVelocity += weight * Time.deltaTime;
            weight = new Vector3(0, mass * gravityValue, 0);
        }
        move += weight * Time.deltaTime;
    }

    IEnumerator Dodge()
    {
        float startTime = Time.time;
        while (Time.time < startTime + dodgeTime)
        {
            characterController.Move(move * dodgeSpeed * Time.deltaTime);
            yield return null;
        }
    }

    #region FeetGrounding

    //private void FixedUpdate()
    //{
    //    if (enableFeetIk == false) return;
    //    if (animator == null) return;

    //    AdjustFeetTarget(ref rightFootPosition, HumanBodyBones.RightFoot);
    //    AdjustFeetTarget(ref leftFootPosition, HumanBodyBones.LeftFoot);

    //    //find and raycast to the ground to find positions
    //    FeetPositionSolver(rightFootPosition, ref rightFootIkPosition, ref rightFootIkRotation); //handle the solver for right foot
    //    FeetPositionSolver(leftFootPosition, ref leftFootIkPosition, ref leftFootIkRotation); // handle the solver for left foot
    //}

    //private void OnAnimatorIK(int layerIndex)
    //{
    //    if (enableFeetIk == false) return;
    //    if (animator == null) return;

    //    MovePelvisHeight();

    //    // right foot ik position and rotation - utilise the pro features in here
    //    animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);

    //    if (useProIkFeature)
    //    {
    //        animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, animator.GetFloat(rightFootAnimVariableName));
    //    }

    //    MoveFeetToIkPoint(AvatarIKGoal.RightFoot, rightFootIkPosition, rightFootIkRotation, ref lastRightFootPositionY);

    //    // left foot ik position and rotation - utilise the pro features in here
    //    animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);

    //    if (useProIkFeature)
    //    {
    //        animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, animator.GetFloat(leftFootAnimVariableName));
    //    }

    //    MoveFeetToIkPoint(AvatarIKGoal.LeftFoot, leftFootIkPosition, leftFootIkRotation, ref lastLeftFootPositionY);
    //}

    #endregion

    #region FeetGroundingMethods

    //private void MoveFeetToIkPoint(AvatarIKGoal foot, Vector3 positionIkHolder, Quaternion rotationIkHolder, ref float lastFootPositionY)
    //{
    //    Vector3 targetIkPosition = animator.GetIKPosition(foot);

    //    if (positionIkHolder != Vector3.zero)
    //    {
    //        targetIkPosition = transform.InverseTransformPoint(targetIkPosition);
    //        positionIkHolder = transform.InverseTransformPoint(positionIkHolder);

    //        float yVariable = Mathf.Lerp(lastFootPositionY, positionIkHolder.y, feetToIkPositionSpeed);
    //        targetIkPosition.y += yVariable;

    //        lastFootPositionY = yVariable;

    //        targetIkPosition = transform.TransformPoint(targetIkPosition);

    //        animator.SetIKRotation(foot, rotationIkHolder);
    //    }

    //    animator.SetIKPosition(foot, targetIkPosition);
    //}

    //private void MovePelvisHeight()
    //{
    //    if (rightFootIkPosition == Vector3.zero || leftFootIkPosition == Vector3.zero || lastPelvisPositionY == 0)
    //    {
    //        lastPelvisPositionY = animator.bodyPosition.y;
    //        return;
    //    }

    //    float lOffsetPosition = leftFootIkPosition.y - transform.position.y;
    //    float rOffsetPosition = rightFootIkPosition.y - transform.position.y;

    //    float totalOffset = (lOffsetPosition < rOffsetPosition) ? lOffsetPosition : rOffsetPosition;

    //    Vector3 newPelvisPosition = animator.bodyPosition + Vector3.up * totalOffset;

    //    newPelvisPosition.y = Mathf.Lerp(lastPelvisPositionY, newPelvisPosition.y, pelvisUpandDownSpeed);

    //    animator.bodyPosition = newPelvisPosition;

    //    lastPelvisPositionY = animator.bodyPosition.y;
    //}

    //private void FeetPositionSolver(Vector3 fromSkyPosition, ref Vector3 feetIkPositions, ref Quaternion feetIkRotations)
    //{
    //    // raycast handling section
    //    RaycastHit feetOutHit;

    //    if (showSolverDebug)
    //    {
    //        Debug.DrawLine(fromSkyPosition, fromSkyPosition + Vector3.down * (raycastDownDistance + heightFromGroundRaycast), Color.yellow);
    //    }

    //    if (Physics.Raycast(fromSkyPosition, Vector3.down, out feetOutHit, raycastDownDistance + heightFromGroundRaycast, environmentLayer))
    //    {
    //        // finding our feet ik positions from the sky position
    //        feetIkPositions = fromSkyPosition;
    //        feetIkPositions.y = feetOutHit.point.y + pelvisOffset;
    //        feetIkRotations = Quaternion.FromToRotation(Vector3.up, feetOutHit.normal) * transform.rotation;

    //        return;
    //    }

    //    feetIkPositions = Vector3.zero;
    //}

    //private void AdjustFeetTarget(ref Vector3 feetPositions, HumanBodyBones foot)
    //{
    //    feetPositions = animator.GetBoneTransform(foot).position;
    //    feetPositions.y = transform.position.y + heightFromGroundRaycast;
    //}

    #endregion
}
