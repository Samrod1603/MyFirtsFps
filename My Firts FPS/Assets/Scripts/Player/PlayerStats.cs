using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public float health , maxHealth;
    [SerializeField] PlayerAnimationManager animationManager;
    public void GetDamage(float damage)
    {
        health -= damage;
    }
    public void Death()
    {
        animationManager.IsDead = true;
        animationManager.Death();
        StartCoroutine(GameManager.Instance.ReturnMainMenu());
    }
}
