using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging; 

public class HeadTracking : MonoBehaviour
{
    List<PointOfInterest> POIs;
    float radiusSqr;

    public Rig headRig; 
    public float radius = 10f;
    public float retargetSpeed = 5f; 
    public Transform target;
    public float MaxAngle = 90f; 
    void Start()
    {
        POIs = FindObjectsOfType<PointOfInterest>().ToList();
        radiusSqr = radius * radius; 
    }
    void Update()
    {
        Transform tracking = null;
        foreach (PointOfInterest poi in POIs)
        {
            Vector3 delta = poi.transform.position - transform.position;
            if (delta.sqrMagnitude < radiusSqr)
            {
                float angle = Vector3.Angle(transform.forward, delta);
                if (angle < MaxAngle)
                {
                    tracking = poi.transform;
                    break;
                }
                
            }
        }

        float rigWeight = 0; 
        Vector3 TargetPos = transform.position + (transform.forward * 2f); 
        if (tracking != null)
        {
            TargetPos = tracking.position;
            rigWeight = 1; 
        }
        target.position = Vector3.Lerp(target.position,TargetPos,Time.deltaTime * retargetSpeed);
        headRig.weight = Mathf.Lerp(headRig.weight, rigWeight, Time.deltaTime * 2) ; 
    }
}
